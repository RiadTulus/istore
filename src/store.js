import {createStore, combineReducers} from 'redux';

const rootReducer = combineReducers({
  displayReducer: {},
});

export default createStore(rootReducer);
