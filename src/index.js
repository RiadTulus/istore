import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {store} from './store';

const rootElement = document.getElementById('root');

render(<Provider store={store}>Hello world</Provider>, rootElement);
